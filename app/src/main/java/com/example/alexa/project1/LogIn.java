package com.example.alexa.project1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LogIn extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

    }


    public void openRegister(View view) {
        Intent intent= new Intent (this, RegisterActivity.class);
        startActivity(intent);
    }

    public void openApp(View view) {


        //compare with bd
        final String username = ((EditText)findViewById(R.id.usernameLogin)).getText().toString();
        final String parola = ((EditText)findViewById(R.id.parolaLogin)).getText().toString();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("utilizator");

        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                boolean found = false;
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                Iterable<DataSnapshot> utilizatori = dataSnapshot.getChildren();
                for(DataSnapshot d : utilizatori){
                    Utilizator value = d.getValue(Utilizator.class);
                    String user = value.getUsername();
                    String pass = value.getParola();
                    if (user.equals(username) && pass.equals(parola)){
                        found = true;
                        break;
                    }

                }
                if (found) {
                    Intent intent = new Intent(getApplicationContext(), ProfilePage.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getApplicationContext(), "Acest cont nu exista", Toast.LENGTH_LONG)
                            .show();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                  Log.w("ERROR", "Failed to read value.", error.toException());
            }
        });




    }
}
