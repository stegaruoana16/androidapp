package com.example.alexa.project1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
private Button BTesteDisponibile;
private Button BIstoricTeste;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stud_meniu);

        BTesteDisponibile = (Button) findViewById(R.id.Stud_BTesteDisponibile);
        BTesteDisponibile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity_stud__teste_disponibile();
            }
        });

        BIstoricTeste = (Button) findViewById(R.id.Stud_BIstoricTeste);
        BIstoricTeste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity_stud_istoric_teste();
            }
        });
    }
    public void openActivity_stud__teste_disponibile(){
        Intent intent = new Intent(this, stud_TesteDisponibile.class);
        startActivity(intent);
    }

    public void openActivity_stud_istoric_teste(){
        Intent intent2 = new Intent(this, Stud_IstoricTeste.class);
        startActivity(intent2);
    }
}
