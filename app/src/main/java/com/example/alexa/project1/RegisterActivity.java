package com.example.alexa.project1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

    }

//    public void openLoginActvivity(){
//        Intent intent= new Intent (this, LogIn.class);
//        startActivity(intent);
//    }

    public void createAccount(View view) {

        String nume = ((EditText)findViewById(R.id.numeRegister)).getText().toString();
        String username = ((EditText)findViewById(R.id.usernameRegister)).getText().toString();
        String parola = ((EditText)findViewById(R.id.parolaRegister)).getText().toString();
        String confirmareParola = ((EditText)findViewById(R.id.confirmareParolaRegister)).getText().toString();

        if (!parola.equals(confirmareParola)){
            Toast.makeText(this, "Parola nu este confirmata", Toast.LENGTH_LONG).show();
            ((EditText)findViewById(R.id.confirmareParolaRegister)).setText("");
            return;
        }

        String functie = ((Spinner)findViewById(R.id.functieRegister)).getSelectedItem().toString();

        Utilizator u = new Utilizator(nume, username, parola, functie);

        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("utilizator");

        //pt a salva mai multi
        DatabaseReference utilizatoriRef = myRef.child(""+u.hashCode());
        utilizatoriRef.setValue(u);

        Toast.makeText(this, "register", Toast.LENGTH_LONG).show();

    }
}
