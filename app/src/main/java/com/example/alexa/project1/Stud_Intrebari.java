package com.example.alexa.project1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Stud_Intrebari extends Activity {
private Button BFinalizareTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stud__intrebari);

        BFinalizareTest = (Button) findViewById(R.id.Stud_BFinalizareTest);
        BFinalizareTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity_stud_test_terminat();
            }
        });
    }

    public void openActivity_stud_test_terminat(){
        Intent intent3 = new Intent(this, Stud_TestTerminat.class);
        startActivity(intent3);
    }
}
