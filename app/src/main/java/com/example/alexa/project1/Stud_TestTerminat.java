package com.example.alexa.project1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Stud_TestTerminat extends Activity {
private Button BMeniu;
private Button BIesire;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stud__test_terminat);

        BMeniu = (Button) findViewById(R.id.Stud_BMeniu);
        BMeniu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMainActivity();
            }
        });

        BIesire = (Button) findViewById(R.id.Stud_BIesire);
        BIesire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                System.exit(1);
            }
        });
    }

    public void openMainActivity(){
        Intent inapoi = new Intent(this, MainActivity.class);
        startActivity(inapoi);
    }
}
