package com.example.alexa.project1;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class TesteleMeleActivity extends AppCompatActivity {

    public class TesteAdapter extends ArrayAdapter<Tests>{

        private int idLayout;
        public TesteAdapter(Context context, int resource, List<Tests> objects) {
            super(context, resource, objects);
            idLayout = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Tests test = getItem(position);
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View view = inflater.inflate(idLayout, null);

            //setam text pt fiecare texview
            TextView numeTest = view.findViewById(R.id.nume_test);
            TextView materieTest = view.findViewById(R.id.materie_test);
            TextView timpTest = view.findViewById(R.id.timp_test);
            TextView intrebariTest = view.findViewById(R.id.intrebari_test);

            numeTest.setText(test.getNume());
            materieTest.setText(test.getMaterie());
            timpTest.setText(""+test.getTimp());
            intrebariTest.setText(""+test.getNrIntrebari());

            return view;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_tests);
//
//        ListView listView = findViewById(R.id.my_tests_activity_list);
//
//        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
//                (this, android.R.layout.simple_list_item_1, AppManager.getMyTestsListComponents());
//
//        listView.setAdapter(arrayAdapter);


        ArrayList<Tests> listaTeste = AppManager.getMyTestsListComponents();
        ListView lw = findViewById(R.id.my_tests_activity_list);


        TesteAdapter adapter = new TesteAdapter(this, R.layout.adaptor_personalizat_teste, listaTeste);
        lw.setAdapter(adapter);



    }
}
