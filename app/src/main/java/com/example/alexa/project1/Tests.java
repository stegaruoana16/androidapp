package com.example.alexa.project1;

public class Tests {

    //todo implements Parcelable?

    private String nume;
    private String materie;
    private int timp;
    private int nrIntrebari;

    public Tests(String nume, String materie, int timp, int nrIntrebari) {
        this.nume = nume;
        this.materie = materie;
        this.timp = timp;
        this.nrIntrebari = nrIntrebari;
    }

    public String getNume() {
        return nume;
    }

    public String getMaterie() {
        return materie;
    }

    public int getTimp() {
        return timp;
    }

    public int getNrIntrebari() {
        return nrIntrebari;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public void setMaterie(String materie) {
        this.materie = materie;
    }

    public void setTimp(int timp) {
        this.timp = timp;
    }

    public void setNrIntrebari(int nrIntrebari) {
        this.nrIntrebari = nrIntrebari;
    }
}
