package com.example.alexa.project1;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

public class ToateTesteleActivity extends AppCompatActivity {

    //TODO itemi clickable

    ListView lw;
    ArrayList<String> materiiList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toate_testele);

        lw = findViewById(R.id.toate_testele_activity_list);

        GetJSONData obiect = new GetJSONData() {

            @Override
            protected void onPostExecute(ArrayList<String> temp) {

                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                        (getApplicationContext(), android.R.layout.simple_list_item_1, temp);

                lw.setAdapter(arrayAdapter);

            }
        };

        obiect.execute("https://api.myjson.com/bins/1f36lq");
    }

//FIXME testele se vor deschide filtrate in alta activitate la click pe itemul cu materii
//        ListView listView = findViewById(R.id.toate_testele_activity_list);
//
//        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
//                (this, android.R.layout.simple_list_item_1, AppManager.getToateTesteleListComponents());
//
//        listView.setAdapter(arrayAdapter);


    public abstract class GetJSONData extends AsyncTask<String, Void, ArrayList<String>> {

        @Override
        protected ArrayList<String> doInBackground(String... strings) {//numar variabil de parametri

            try {

                URL url = new URL(strings[0]);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();

                InputStream stream = http.getInputStream();

                BufferedReader reader  = new BufferedReader((new InputStreamReader((stream))));
                StringBuilder builder = new StringBuilder();

                String linie="";
                while((linie=reader.readLine())!=null){
                    builder.append(linie);
                }

                JSONObject object = new JSONObject(builder.toString());
                JSONObject teste = object.getJSONObject(("teste"));
                JSONArray materii = teste.getJSONArray(("materii"));

                for(int i=0; i < materii.length(); i++){
                    JSONObject materie = (JSONObject)materii.get(i);
                    String nume = materie.getString("nume");
                    String an = materie.getString("an");
                    String semestrul = materie.getString("semestrul");

                    String rezultat = nume + " \n   an: " + an + " \n   semestrul: " + semestrul;
                    materiiList.add(rezultat);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return materiiList;
        }


    }









}
