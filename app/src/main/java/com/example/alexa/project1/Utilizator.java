package com.example.alexa.project1;

public class Utilizator {

    private int id;
    private String nume;
    private String username;
    private String parola;
    private String functie;

    public String getNume() {
        return nume;
    }

    public String getUsername() {
        return username;
    }

    public String getParola() {
        return parola;
    }

    public String getFunctie() {
        return functie;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setParola(String parola) {
        this.parola = parola;
    }

    public void setFunctie(String functie) {
        this.functie = functie;
    }

    public Utilizator(String nume, String username, String parola, String functie) {
        this.nume = nume;
        this.username = username;
        this.parola = parola;
        this.functie = functie;
    }

    public Utilizator(int id, String nume, String username, String parola, String functie) {
        this.id = id;
        this.nume = nume;
        this.username = username;
        this.parola = parola;
        this.functie = functie;
    }

    public Utilizator() {
        this.nume = "";
        this.username = "";
        this.parola = "";
        this.functie = "";
    }
}
