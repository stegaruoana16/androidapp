package com.example.alexa.project1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class stud_TesteDisponibile extends Activity {
private Button BInapoiMeniu;
private Button BTestulDisponibil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stud__teste_disponibile);

        BInapoiMeniu = (Button) findViewById(R.id.Stud_BInapoi);
        BInapoiMeniu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMainActivity();
            }
        });

        BTestulDisponibil = (Button) findViewById(R.id.Stud_BContinua);
        BTestulDisponibil.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                openActivity_stud_testul_disponibil();
            }
        });
    }
    public void openMainActivity(){
        Intent inapoi = new Intent(this, MainActivity.class);
        startActivity(inapoi);
    }

    public void openActivity_stud_testul_disponibil(){
        Intent continua = new Intent(this, stud_TestulDisponibil.class);
        startActivity(continua);
    }
}
