package com.example.alexa.project1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class stud_TestulDisponibil extends Activity {
private Button BStartTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stud__testul_disponibil);

        BStartTest = (Button) findViewById(R.id.Stud_BStartTest);
        BStartTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity_stud_intrebari();
            }
        });
    }
    public void openActivity_stud_intrebari(){
        Intent intent = new Intent(this, Stud_Intrebari.class);
        startActivity(intent);
    }
}
